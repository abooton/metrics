"""
Create an uncompressed FF file, using mule.

Core code originally from sworsley.

Called from "./make_ff_into_uncompressed.sh"
See there for essential calling environment setup.

"""

import mule

import os
from pathlib import Path

_DATADIR_VARNAME = 'BENCHMARK_DATA'  # For local runs

benchmark_data_dir = os.environ.get(_DATADIR_VARNAME, None)
if benchmark_data_dir is None:
    msg = ('Benchmark data dir is not defined : "${}" must be set.')
    raise(ValueError(msg.format(_DATADIR_VARNAME)))

benchmark_data_dir = Path(benchmark_data_dir)

in_path = str(benchmark_data_dir / 'umglaa_pb000-theta')
out_path = str(benchmark_data_dir / 'umglaa_pb000-theta.uncompressed')

ff = mule.FieldsFile.from_file(in_path)
ff_out = ff.copy()

for field in ff.fields:
    if field.lbrel in (2,3):
        dp = mule.ArrayDataProvider(field.get_data())
        new_field = field.copy()
        new_field.set_data_provider(dp)
        new_field.lbpack = 0
        new_field.bacc = 0
        ff_out.fields.append(new_field)

ff_out.to_file(out_path)
