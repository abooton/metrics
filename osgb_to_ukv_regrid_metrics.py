"""
Metrics for regridding high-res data on an OSGB grid onto a lower-res UKV grid,
using the area-weighted regridding scheme.

Note that area-weighted regridding cannot currently perform cross-CRS
regridding, so the metrics below all follow a two-step process that uses
an intermediate linear regrid.

This statement is correct at time of writing, however there is a plan to add
cross-CRS regridding to the area-weighted scheme.

"""

import os

import biggus
import iris
from iris.analysis import Linear, AreaWeighted
import numpy as np

from tehuti import TimeMetric


SOURCE = '/project/atk/_tmp_data_store/avd_sprint/ite_veg_fraction/ukv_grid'


def intermediate_grid_cube(src_cube, target_cube, shape=None):
    """
    Generate a grid cube that covers the same area as the source cube only
    that it has the coordinate system of the target cube and specified shape.

    See https://gist.github.com/cpelley/9369e4c21afc24769ed9.

    """
    if shape is None:
        shape = src_cube.shape

    src_x_coord = src_cube.coord(axis='y', dim_coords=True)
    src_y_coord = src_cube.coord(axis='x', dim_coords=True)
    dim_x = src_cube.coord_dims(src_y_coord)[0]
    dim_y = src_cube.coord_dims(src_x_coord)[0]
    dim_y, dim_x = np.argsort([dim_y, dim_x])

    dtype = target_cube.lazy_data().dtype
    data = biggus.ConstantArray(shape, dtype=dtype)
    grid_cube = iris.cube.Cube(data)

    src_x_points = src_x_coord.points
    if src_x_coord.has_bounds():
        src_x_points = src_x_coord.bounds
    src_y_points = src_y_coord.points
    if src_y_coord.has_bounds():
        src_y_points = src_y_coord.bounds

    # Determine bounding box around source data.
    src_bounds = ((src_x_points.min(), src_x_points.max()),
                  (src_y_points.min(), src_y_points.max()))
    src_x_points = np.array([src_bounds[0][0], src_bounds[0][0],
                             src_bounds[0][1], src_bounds[0][1]])
    src_y_points = np.array([src_bounds[1][0], src_bounds[1][1],
                             src_bounds[1][1], src_bounds[1][0]])

    src_crs = src_x_coord.coord_system.as_cartopy_crs()
    tgt_crs = target_cube.coord(axis='x').coord_system.as_cartopy_crs()
    xyz = tgt_crs.transform_points(src_crs, src_x_points, src_y_points)
    x, y = xyz[..., 0], xyz[..., 1]

    # Generate points by utilising bounds if available.
    if src_x_coord.has_bounds():
        x_bound = np.linspace(x.min(), x.max(),
                              endpoint=True, num=shape[dim_x]+1)
        x_bounds = np.array([x_bound[:-1], x_bound[1:]]).T
        x_points = x_bounds.mean(axis=1)
    else:
        x_points = np.linspace(x.min(), x.max(), num=shape[dim_x])

    if src_y_coord.has_bounds():
        y_bound = np.linspace(y.min(), y.max(),
                              endpoint=True, num=shape[dim_y]+1)
        y_bounds = np.array([y_bound[:-1], y_bound[1:]]).T
        y_points = y_bounds.mean(axis=1)
    else:
        y_points = np.linspace(y.min(), y.max(), num=shape[dim_y])

    # Add these dimensions to our grid cube
    grid_x_coord = target_cube.coord(axis='x').copy(x_points)
    grid_x_coord.bounds = x_bounds
    grid_y_coord = target_cube.coord(axis='y').copy(y_points)
    grid_y_coord.bounds = y_bounds
    grid_cube.add_dim_coord(grid_y_coord, dim_y)
    grid_cube.add_dim_coord(grid_x_coord, dim_x)

    return grid_cube


def setup_2_2(context):
    src = '2_2_source.nc'
    dest = '2_2_target.nc'
    context.src = iris.load_cube(os.path.join(SOURCE, src))
    context.dest = iris.load_cube(os.path.join(SOURCE, dest))
    context.inter = intermediate_grid_cube(context.src, context.dest)
    context.inter_kw = {}
    context.dest_kw = {}


def setup_8_8(context):
    src = '8_8_source.nc'
    dest = '8_8_target.nc'
    context.src = iris.load_cube(os.path.join(SOURCE, src))
    context.dest = iris.load_cube(os.path.join(SOURCE, dest))
    context.inter = intermediate_grid_cube(context.src, context.dest)
    context.inter_kw = {}
    context.dest_kw = {}


def setup_15_15(context):
    src = '15_15_source.nc'
    dest = '15_15_target.nc'
    context.src = iris.load_cube(os.path.join(SOURCE, src))
    context.dest = iris.load_cube(os.path.join(SOURCE, dest))
    context.inter = intermediate_grid_cube(context.src, context.dest)
    context.inter_kw = {}
    context.dest_kw = {}


def setup_24_24(context):
    src = '24_24_source.nc'
    dest = '24_24_target.nc'
    context.src = iris.load_cube(os.path.join(SOURCE, src))
    context.dest = iris.load_cube(os.path.join(SOURCE, dest))
    context.inter = intermediate_grid_cube(context.src, context.dest)
    context.inter_kw = {}
    context.dest_kw = {}


def setup_32_32(context):
    src = '32_32_source.nc'
    dest = '32_32_target.nc'
    context.src = iris.load_cube(os.path.join(SOURCE, src))
    context.dest = iris.load_cube(os.path.join(SOURCE, dest))
    context.inter = intermediate_grid_cube(context.src, context.dest)
    context.inter_kw = {}
    context.dest_kw = {}


def two_stage_regrid(context):
    intermediate = context.src.regrid(context.inter,
                                      Linear(**context.inter_kw))
    intermediate.regrid(context.dest, AreaWeighted(**context.dest_kw))


metrics = [
    # TimeMetric(two_stage_regrid, setup_2_2, repeat=1,
    #            name='two stage regrid, 2x2 src and target grids'),
    TimeMetric(two_stage_regrid, setup_8_8, repeat=3,
               name='two stage regrid, 8x8 source and target grids'),
    TimeMetric(two_stage_regrid, setup_15_15, repeat=5,
               name='two stage regrid, 15x15 source and target grids'),
    TimeMetric(two_stage_regrid, setup_24_24, repeat=10,
               name='two stage regrid, 24x24 source and target grids'),
    TimeMetric(two_stage_regrid, setup_32_32, repeat=10,
               name='two stage regrid, 32x32 source and target grids'),
    ]
