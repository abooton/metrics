Clone the metrics repository
----------------------------

Clone the Bitbucket metrics git repository:

    $ git clone git@bitbucket.org:metoffice/metrics.git


Install conda
-------------

The following installation step only requires to be performed once.

    $ wget --quiet https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh -O miniconda.sh
    $ bash miniconda.sh -b -p $(pwd)/miniconda3
    $ export PATH="$(pwd)/miniconda3/bin:${PATH}"


Create a conda nox environment
------------------------------

The following configuration step only requires to be performed once.

    $ conda update conda
    $ conda env create --file=nox.yml


Enable nox conda environment
----------------------------

The following activation step requires to be performed once per new shell session.

    $ . activate nox


Discover performance metrics
----------------------------

Use nox to discover and execute the desired perfomance metrics. Nox must be run in
the same directory as the noxfile.py

To list the performance metrics known to nox:

    $ nox --list

To execute all performance metrics:

    $ nox

To run a specific performance metric:

    $ nox --session <name>

For further details on how to use nox:

    $ nox --help

Also see https://nox.thea.codes/en/stable/
