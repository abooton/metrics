"""
Tehuti timeit metrics for the Iris area-weighted regrid scheme.

"""

from iris.analysis import AreaWeighted
import iris.coord_systems
from tehuti import TimeMetric

from iris_metrics import yx_cube, zyx_cube


# Globals.
SRC_TINY = (20, 18)
LIM_SRC_TINY = (8, 6)
DEST_TINY = (12, 9)
LIM_DEST_TINY = (9, 6)

SRC_LARGE = (1000, 750)
LIM_SRC_LARGE = (800, 600)
DEST_LARGE = (600, 450)
LIM_DEST_LARGE = (900, 600)

SRC_GIANT = (21600, 19440)
DEST_GIANT = (12960, 9720)
SRC_HUGE = (86400, 77760)
DEST_HUGE = (51840, 38880)


# ----------------------------------------------------------------------------
# Simple regrid operations. Parameters:
#   * src grid: global; tiny to huge resolution,
#   * dest grid: global; tiny to huge resolution,
#   * CS: GeogCS.

def simple_regrid_setup_tiny(context):
    context.src = yx_cube(SRC_TINY, bounds=True)
    context.dest = yx_cube(DEST_TINY, bounds=True)
    context.kwargs = {}


def simple_regrid_setup_large(context):
    context.src = yx_cube(SRC_LARGE, bounds=True)
    context.dest = yx_cube(DEST_LARGE, bounds=True)
    context.kwargs = {}


def simple_regrid_setup_giant(context):
    # Mimic a 1' dataset.
    context.src = yx_cube(SRC_GIANT, bounds=True)
    context.dest = yx_cube(DEST_GIANT, bounds=True)
    context.kwargs = {}


def simple_regrid_setup_huge(context):
    # Mimic a 15" dataset.
    context.src = yx_cube(SRC_HUGE, bounds=True)
    context.dest = yx_cube(DEST_HUGE, bounds=True)
    context.kwargs = {}


# ----------------------------------------------------------------------------
# Regrid operations with large differences between src and target grid sizes.
# Parameters:
#   * src grid: global; tiny or giant resolution,
#   * dest grid: global; large or tiny resolution,
#   * CS: GeogCS, RotatedGeogCS.

def regrid_setup_giant_to_tiny(context):
    context.src = yx_cube(SRC_GIANT, bounds=True)
    context.dest = yx_cube(DEST_TINY, bounds=True)
    context.kwargs = {}


def regrid_setup_tiny_to_large(context):
    context.src = yx_cube(DEST_TINY, bounds=True)
    context.dest = yx_cube(SRC_LARGE, bounds=True)
    context.kwargs = {}


def rotated_regrid_setup_giant_to_tiny(context):
    context.src = yx_cube(SRC_GIANT, bounds=True,
                          cs=iris.coord_systems.RotatedGeogCS(30., 30.))
    context.dest = yx_cube(DEST_TINY, bounds=True,
                           cs=iris.coord_systems.RotatedGeogCS(30., 30.))
    context.kwargs = {}


def rotated_regrid_setup_tiny_to_large(context):
    context.src = yx_cube(DEST_TINY, bounds=True,
                          cs=iris.coord_systems.RotatedGeogCS(30., 30.))
    context.dest = yx_cube(SRC_LARGE, bounds=True,
                           cs=iris.coord_systems.RotatedGeogCS(30., 30.))
    context.kwargs = {}


# ----------------------------------------------------------------------------
# Regrid operations. Parameters:
#   * src grid: global; tiny to huge resolution,
#   * dest grid: global; tiny to huge resolution,
#   * CS: RotatedGeogCS.

def rotated_regrid_setup_tiny(context):
    context.src = yx_cube(SRC_TINY, bounds=True,
                          cs=iris.coord_systems.RotatedGeogCS(30., 30.))
    context.dest = yx_cube(DEST_TINY, bounds=True,
                           cs=iris.coord_systems.RotatedGeogCS(30., 30.))
    context.kwargs = {}


def rotated_regrid_setup_large(context):
    context.src = yx_cube(SRC_LARGE, bounds=True,
                          cs=iris.coord_systems.RotatedGeogCS(30., 30.))
    context.dest = yx_cube(DEST_LARGE, bounds=True,
                           cs=iris.coord_systems.RotatedGeogCS(30., 30.))
    context.kwargs = {}


def rotated_regrid_setup_giant(context):
    # Mimic a 1' dataset.
    context.src = yx_cube(SRC_GIANT, bounds=True,
                          cs=iris.coord_systems.RotatedGeogCS(30., 30.))
    context.dest = yx_cube(DEST_GIANT, bounds=True,
                           cs=iris.coord_systems.RotatedGeogCS(30., 30.))
    context.kwargs = {}


def rotated_regrid_setup_huge(context):
    # Mimic a 15" dataset.
    context.src = yx_cube(SRC_HUGE, bounds=True,
                          cs=iris.coord_systems.RotatedGeogCS(30., 30.))
    context.dest = yx_cube(DEST_HUGE, bounds=True,
                           cs=iris.coord_systems.RotatedGeogCS(30., 30.))
    context.kwargs = {}


# ----------------------------------------------------------------------------
# Regrid operations across coord systems. Parameters:
#   * src grid: global; tiny or large resolution,
#   * dest grid: global; tiny or large resolution,
#   * CS: GeogCS, RotatedGeogCS.

def to_rotated_regrid_setup_tiny(context):
    context.src = yx_cube(SRC_TINY, bounds=True)
    context.dest = yx_cube(DEST_TINY, bounds=True,
                           cs=iris.coord_systems.RotatedGeogCS(30., 30.))
    context.kwargs = {}


def to_rotated_regrid_setup_large(context):
    context.src = yx_cube(SRC_LARGE, bounds=True)
    context.dest = yx_cube(DEST_LARGE, bounds=True,
                           cs=iris.coord_systems.RotatedGeogCS(30., 30.))
    context.kwargs = {}


def from_rotated_regrid_setup_tiny(context):
    context.src = yx_cube(SRC_TINY, bounds=True,
                          cs=iris.coord_systems.RotatedGeogCS(30., 30.))
    context.dest = yx_cube(DEST_TINY, bounds=True)
    context.kwargs = {}


def from_rotated_regrid_setup_large(context):
    context.src = yx_cube(SRC_LARGE, bounds=True,
                          cs=iris.coord_systems.RotatedGeogCS(30., 30.))
    context.dest = yx_cube(DEST_LARGE, bounds=True)
    context.kwargs = {}


# ----------------------------------------------------------------------------
# Regrid operations onto limited-area target domain.
# Parameters:
#   * src grid: global; tiny or giant resolution,
#   * dest grid: limited-area; tiny or large resolution,
#   * CS: GeogCS, RotatedGeogCS.

def to_limited_area_regrid_setup_tiny_to_tiny(context):
    context.src = yx_cube(SRC_TINY, bounds=True)
    context.dest = yx_cube(LIM_DEST_TINY, lat=(-65, 65), lon=(-140, 140),
                           bounds=True)
    context.kwargs = {}


def to_limited_area_regrid_setup_large_to_large(context):
    context.src = yx_cube(SRC_LARGE, bounds=True)
    context.dest = yx_cube(LIM_DEST_LARGE, lat=(-65, 65), lon=(-140, 140),
                           bounds=True)
    context.kwargs = {}


def to_limited_area_regrid_setup_tiny_to_large(context):
    context.src = yx_cube(SRC_TINY, bounds=True)
    context.dest = yx_cube(LIM_DEST_LARGE, lat=(-65, 65), lon=(-140, 140),
                           bounds=True)
    context.kwargs = {}


def to_limited_area_regrid_setup_large_to_tiny(context):
    context.src = yx_cube(SRC_LARGE, bounds=True)
    context.dest = yx_cube(LIM_DEST_TINY, lat=(-65, 65), lon=(-140, 140),
                           bounds=True)
    context.kwargs = {}


def to_limited_area_rotated_regrid_setup_large_to_large(context):
    context.src = yx_cube(SRC_LARGE, bounds=True,
                          cs=iris.coord_systems.RotatedGeogCS(30., 30.))
    context.dest = yx_cube(LIM_DEST_LARGE, lat=(-65, 65), lon=(-140, 140),
                           bounds=True,
                           cs=iris.coord_systems.RotatedGeogCS(30., 30.))
    context.kwargs = {}


def to_limited_area_from_rotated_regrid_setup_large_to_large(context):
    context.src = yx_cube(SRC_LARGE, bounds=True,
                          cs=iris.coord_systems.RotatedGeogCS(30., 30.))
    context.dest = yx_cube(LIM_DEST_LARGE, lat=(-65, 65), lon=(-140, 140),
                           bounds=True)
    context.kwargs = {}


def to_limited_area_to_rotated_regrid_setup_large_to_large(context):
    context.src = yx_cube(SRC_LARGE, bounds=True)
    context.dest = yx_cube(LIM_DEST_LARGE, lat=(-65, 65), lon=(-140, 140),
                           bounds=True,
                           cs=iris.coord_systems.RotatedGeogCS(30., 30.))
    context.kwargs = {}


# ----------------------------------------------------------------------------
# Regrid operations on cubes with multiple vertical levels. Parameters:
#   * src grid: global or limited area; tiny to large resolution,
#   * dest grid: global or limited area; tiny to large resolution,
#   * CS: GeogCS or RotatedGeogCS,
#   * vertical levels.

def vertical_levels_regrid_setup_tiny(context):
    context.src = zyx_cube(shape=SRC_TINY, bounds=True)
    context.dest = zyx_cube(shape=DEST_TINY, bounds=True)
    context.kwargs = {}


def vertical_levels_regrid_setup_large(context):
    context.src = zyx_cube(shape=SRC_LARGE, bounds=True)
    context.dest = zyx_cube(shape=DEST_LARGE, bounds=True)
    context.kwargs = {}


def vertical_levels_rotated_regrid_setup_large(context):
    context.src = zyx_cube(shape=SRC_LARGE, bounds=True,
                           cs=iris.coord_systems.RotatedGeogCS(30., 30.))
    context.dest = zyx_cube(shape=DEST_LARGE, bounds=True,
                            cs=iris.coord_systems.RotatedGeogCS(30., 30.))
    context.kwargs = {}


def vertical_levels_from_rotated_regrid_setup_large(context):
    context.src = zyx_cube(shape=SRC_LARGE, bounds=True,
                           cs=iris.coord_systems.RotatedGeogCS(30., 30.))
    context.dest = zyx_cube(shape=DEST_LARGE, bounds=True)
    context.kwargs = {}


def vertical_levels_to_rotated_regrid_setup_large(context):
    context.src = zyx_cube(shape=SRC_LARGE, bounds=True)
    context.dest = zyx_cube(shape=DEST_LARGE, bounds=True,
                            cs=iris.coord_systems.RotatedGeogCS(30., 30.))
    context.kwargs = {}


def vertical_levels_from_limited_area_regrid_setup_large(context):
    context.src = zyx_cube(shape=LIM_SRC_LARGE, lat=(-65, 65), lon=(-140, 140),
                           bounds=True)
    context.dest = zyx_cube(shape=DEST_LARGE, bounds=True)
    context.kwargs = {}


def vertical_levels_to_limited_area_regrid_setup_large(context):
    context.src = zyx_cube(shape=SRC_LARGE, bounds=True)
    context.dest = zyx_cube(shape=LIM_DEST_LARGE,
                            lat=(-65, 65), lon=(-140, 140),
                            bounds=True)
    context.kwargs = {}


# ----------------------------------------------------------------------------
def regrid(context):
    """Runs an Area Weighted regrid operation with context parameters."""
    regridder = AreaWeighted
    context.src.regrid(context.dest, regridder(**context.kwargs))


metrics = [
    TimeMetric(regrid, simple_regrid_setup_tiny, repeat=10,
               name='simple regrid (tiny)'),
    TimeMetric(regrid, simple_regrid_setup_large, repeat=3,
               name='simple regrid (large)'),
    # TimeMetric(regrid, simple_regrid_setup_giant, repeat=1,
    #            name='simple regrid (giant)'),
    # TimeMetric(regrid, simple_regrid_setup_huge, repeat=1,
    #            name='simple regrid (huge)'),
    # ---
    TimeMetric(regrid, rotated_regrid_setup_tiny, repeat=10,
               name='rotated regrid (tiny)'),
    TimeMetric(regrid, rotated_regrid_setup_large, repeat=3,
               name='rotated regrid (large)'),
    # TimeMetric(regrid, rotated_regrid_setup_giant, repeat=1,
    #            name='rotated regrid (giant)'),
    # TimeMetric(regrid, rotated_regrid_setup_huge, repeat=1,
    #            name='rotated regrid (huge)'),
    # ---
    TimeMetric(regrid, regrid_setup_giant_to_tiny, repeat=3,
               name='regrid (giant - tiny)'),
    TimeMetric(regrid, regrid_setup_tiny_to_large, repeat=3,
               name='rotated regrid (tiny - large)'),
    TimeMetric(regrid, rotated_regrid_setup_giant_to_tiny, repeat=3,
               name='rotated regrid (giant - tiny)'),
    TimeMetric(regrid, rotated_regrid_setup_tiny_to_large, repeat=3,
               name='rotated regrid (tiny - large)'),
    # ---
    # TimeMetric(regrid, to_rotated_regrid_setup_tiny, repeat=100,
    #            name='regrid (tiny, to rotated)'),
    # TimeMetric(regrid, to_rotated_regrid_setup_large, repeat=10,
    #            name='regrid (large, to rotated)'),
    # TimeMetric(regrid, from_rotated_regrid_setup_tiny, repeat=100,
    #            name='regrid (tiny, from rotated)'),
    # TimeMetric(regrid, from_rotated_regrid_setup_large, repeat=10,
    #            name='regrid (large, from rotated)'),
    # ---
    TimeMetric(regrid, to_limited_area_regrid_setup_tiny_to_tiny, repeat=10,
               name='regrid to limited area (tiny - tiny)'),
    TimeMetric(regrid, to_limited_area_regrid_setup_large_to_large, repeat=3,
               name='regrid to limited area (large - large)'),
    TimeMetric(regrid, to_limited_area_regrid_setup_tiny_to_large, repeat=3,
               name='regrid to limited area (tiny - large)'),
    TimeMetric(regrid, to_limited_area_regrid_setup_large_to_tiny, repeat=3,
               name='regrid to limited area (large - tiny)'),
    TimeMetric(regrid, to_limited_area_rotated_regrid_setup_large_to_large,
               repeat=3, name='rotated regrid to limited area (tiny - tiny)'),
    # TimeMetric(regrid,
    #            to_limited_area_from_rotated_regrid_setup_large_to_large,
    #            repeat=3, name='regrid to limited area (tiny, from rotated)'),
    #TimeMetric(regrid, to_limited_area_to_rotated_regrid_setup_large_to_large,
    #            repeat=3, name='regrid to limited area (tiny, to rotated)'),
    # ---
    TimeMetric(regrid, vertical_levels_regrid_setup_tiny, repeat=10,
               name='vertical levels regrid (tiny - tiny)'),
    TimeMetric(regrid, vertical_levels_regrid_setup_large, repeat=3,
               name='vertical levels regrid (large - large)'),
    TimeMetric(regrid, vertical_levels_rotated_regrid_setup_large, repeat=3,
               name='vertical levels rotated regrid (large)'),
    # TimeMetric(regrid, vertical_levels_from_rotated_regrid_setup_large,
    #            repeat=3, name='vertical levels regrid (large, from rotated)'),
    # TimeMetric(regrid, vertical_levels_to_rotated_regrid_setup_large, repeat=3,
    #            name='vertical levels regrid (large, to rotated)'),
    TimeMetric(regrid, vertical_levels_from_limited_area_regrid_setup_large,
               repeat=3,
               name='vertical levels regrid from limited area (large)'),
    TimeMetric(regrid, vertical_levels_to_limited_area_regrid_setup_large,
               repeat=3,
               name='vertical levels regrid to limited area (large)'),
    ]
