import os.path
import warnings

import iris
import iris.coord_systems
import iris.fileformats.ff as ff
import numpy as np
from tehuti import LineCountMetric, PylintMetric, TimeMetric


# Small PP load
def pp_load(context):
    iris.load('/data/local/dataZoo/PP/aPPglob1/global.pp')


# Large FF load
def structured_ff_load_setup(context):
    """
    Adds a 'filenames' attribute to the context containing the
    paths to a number of 78GB structured FieldsFiles.

    """
    root_dir = '/project/badc/hiresgw'
    filenames = ['xjanpa.ph19921001',
                 'xjanpa.ph19921011',
                 'xjanpa.ph19921121',
                 'xjanpa.ph19921201',
                 'xjanpa.ph19921211',
                 'xjanpa.ph19930101']
    context.filenames = [os.path.join(root_dir, filename) for filename in
                         filenames]


def structured_ff_load(context):
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        # Limit the number of files to reduce the time taken to run
        # the metric. Experiments suggest that the load time for each
        # file is comparable.
        nfiles = 1
        for filename in context.filenames[:nfiles]:
            iris.load(filename)


# FF/PP load
def small_ff_pp_setup(context):
    from iris.tests import get_data_path
    context.filename = get_data_path(('FF', 'n48_multi_field'))


def large_ff_pp_setup(context):
    context.filename = '/project/badc/hiresgw/xjanpa.ph19921001'


def instantiate_FF2PP_object(context):
    ff.FF2PP(context.filename)


def FF2PP_fields_iteration(context):
    """
    Iterate over the fields in a :class:`iris.fileformats.ff.FF2PP` object.

    """
    for field in ff.FF2PP(context.filename):
        pass


def FF2PP__extract_field_iteration(context):
    """
    This iterates over the generator from :meth:`FF2PP._extract_field()`.

    This performs the benchmark without calling
    :func:`iris.fileformats.pp._interpret_fields`.

    """
    for field in ff.FF2PP(context.filename)._extract_field():
        pass


# Small cube regridding
def yx_cube(shape=(600, 800), lat=(-90, 90), lon=(-180, 180),
            cs=iris.coord_systems.GeogCS(6371229), bounds=False):

    cube = iris.cube.Cube(np.arange(np.prod(shape),
                                    dtype=np.float32).reshape(shape))

    coord = iris.coords.DimCoord(points=np.linspace(lat[0], lat[1], shape[0]),
                                 standard_name='latitude',
                                 units='degrees',
                                 coord_system=cs)
    if bounds:
        coord.guess_bounds()
    cube.add_dim_coord(coord, 0)
    coord = iris.coords.DimCoord(points=np.linspace(lon[0], lon[1], shape[1]),
                                 standard_name='longitude',
                                 units='degrees',
                                 coord_system=cs)
    if bounds:
        coord.guess_bounds()
    cube.add_dim_coord(coord, 1)
    return cube


def zyx_cube(levels=30, **kwargs):
    levels_cubelist = []
    for l in range(levels):
        cube = yx_cube(**kwargs)
        coord = iris.coords.AuxCoord(points=np.array(l),
                                     long_name='z',
                                     units='m')
        cube.add_aux_coord(coord)
        levels_cubelist.append(cube)
    levels_cubelist = iris.cube.CubeList(levels_cubelist)
    return levels_cubelist.merge_cube()


def regrid_small_to_small_setup(context):
    from iris.analysis import Linear

    context.regrid = Linear
    context.src = yx_cube((200, 180))
    context.dest = yx_cube((120, 90))


def regrid(context):
    context.src.regrid(context.dest, context.regrid())


metrics = [
    TimeMetric(structured_ff_load, structured_ff_load_setup, repeat=1),
    TimeMetric(pp_load),
    TimeMetric(regrid, regrid_small_to_small_setup, repeat=2,
               name='regrid-small-to-small'),
    # LineCountMetric('/data/local/ithr/git/iris/lib/iris/cube.py'),
    PylintMetric('iris.fileformats.pp_rules'),
    TimeMetric(instantiate_FF2PP_object, small_ff_pp_setup,
               name='instantiate_FF2PP_small'),
    TimeMetric(FF2PP_fields_iteration, small_ff_pp_setup,
               name='FF2PP_iteration_small'),
    TimeMetric(FF2PP__extract_field_iteration, small_ff_pp_setup,
               name='FF2PP__extract_field_iteration_small'),
    TimeMetric(instantiate_FF2PP_object, large_ff_pp_setup,
               name='instantiate_FF2PP_large'),
    TimeMetric(FF2PP_fields_iteration, large_ff_pp_setup,
               repeat=3, name='FF2PP_iteration_large'),
    TimeMetric(FF2PP__extract_field_iteration, large_ff_pp_setup,
               repeat=3, name='FF2PP__extract_field_iteration_large'),
    ]
