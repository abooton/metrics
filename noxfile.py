"""
Perform test automation with nox.

For further details, see https://nox.thea.codes/en/stable/#

Reference:
  - https://docs.github.com/en/free-pro-team@latest/rest/overview/endpoints-available-for-github-apps

"""

import codecs
import colorama as cr
from datetime import datetime
import json
import logging
import os
from pathlib import Path
from subprocess import check_output

from beautifultable import BeautifulTable
import nox
import requests


#: Default to reusing any pre-existing nox environments.
nox.options.reuse_existing_virtualenvs = True

#: The Python version of the nox environment.
PY_VER = os.environ.get("PY_VER", ["3.6", "3.7"])

#: ASV base configuration directory for CPerf and SPerf metrics.
ASV_BASE_DIR = (Path(__file__).parent / "asv_projects" / "csperf").resolve(strict=True)

#: ASV cache flag for current commit sha.
ASV_COMMIT_FLAG = "asv-commit"

#: Packages to be installed in the nox session environment.
DEFAULT_PACKAGES = ["asv", "pip"]

#: Conda channel to source nox session environment packages.
CONDA_CHANNEL = "conda-forge"

#: Symbol representing the latest commit of a branch i.e., HEAD.
LATEST_COMMIT = "latest"

#: nox/ASV cache filename.
CACHE_FILE = "cache.json"

#: Default directory for ASV metric data.
DEFAULT_DATA_DIR = Path(__file__).parent / "data"

#: Environment hook for user specifying the ASV metric data directory.
DATA_CACHE_DIR = Path(os.environ.get("NGVAT_SYNTHETIC_DATA_CACHEDIR", DEFAULT_DATA_DIR))


#: Set the urllib3 logging level (dependency of requests).
logging.getLogger("urllib3").setLevel(logging.WARNING)


#: Look-up table for packages with dependencies to be managed.
lookup = {
    "iris": {
        "endpoint": "https://api.github.com/repos/SciTools/iris",
        "arg": {
            "branch": None,
            "commit": None,
        },
        "flag": {
            "branch": "iris-branch",
            "commit": "iris-commit",
        },
        "default": {
            "branch": "ugrid",
            "commit": LATEST_COMMIT,
        },
    },
    "iris-ugrid": {
        "endpoint": "https://api.github.com/repos/SciTools-incubator/iris-ugrid",
        "arg": {
            "branch": None,
            "commit": None,
        },
        "flag": {
            "branch": "ugrid-branch",
            "commit": "ugrid-commit",
        },
        "default": {
            "branch": "master",
            "commit": LATEST_COMMIT,
        },
    },
}


def pip_install(session, packages):
    """
    Install the pip packages into the
    associated nox session environment.

    Parameters
    ----------
    session : object
        A `nox.sessions.Session` object.
    packages : None or str or list of str
        pip packages to be installed into
        the associated nox session environment.

    """
    # Determine list of packages already intalled
    # and known to pip.
    pip = str((Path(session.bin) / "pip").resolve(strict=True))
    command = [pip, "list", "--format", "freeze"]
    output = check_output(command).decode("utf-8").split()
    installed = dict([spec.split("==") for spec in output])

    for package in packages:
        if package not in installed:
            session.install(package)


def conda_install(session, packages):
    """
    Install the conda packages into the
    associated nox session environment.

    Parameters
    ----------
    session : object
        A `nox.sessions.Session` object.
    packages : None or str or list of str
        conda packages to be installed into
        the associated nox session environment.

    """

    def installed():
        result = list(meta_dir.glob(f"**/{package}-[0-9]*"))
        return len(result) == 1

    # Associated conda-meta directory of the nox session environment.
    meta_dir = (Path(session.virtualenv.location) / "conda-meta").resolve(strict=True)

    for package in packages:
        if not installed():
            session.conda_install(f"--channel={CONDA_CHANNEL}", package)


def install(session, packages=None):
    """
    Install the conda and pip packages into the
    associated nox session environment.

    Parameters
    ----------
    session : object
        A `nox.sessions.Session` object.
    packages : None or str or list of str
        pip or conda packages to be installed into
        the associated nox session environment.

    """
    if packages is None:
        packages = ()
    elif isinstance(packages, str):
        packages = (packages,)
    else:
        packages = tuple(packages)

    packages = tuple(DEFAULT_PACKAGES) + packages
    conda_packages = set()
    pip_packages = set()

    # Seperate conda and pip packages.
    for package in packages:
        if package.startswith("pip:"):
            _, package = package.split(":")
            pip_packages.add(package)
        else:
            conda_packages.add(package)

    if conda_packages:
        conda_install(session, sorted(conda_packages))

    if pip_packages:
        pip_install(session, sorted(pip_packages))


def parse_cla(package, args):
    """
    Update the look-up table with relevant "iris" CLAs.

    Parameters
    ----------
    package : dict
        The relevant section of the look-up table for
        a specific package i.e., "iris".

    Notes
    -----
    It's only necessary to parse the special CLAs for
    "iris", which is a dependency of "iris-ugrid". ASV
    is controlling "iris-ugrid" as the top level package
    to be benchmarked.

    Returns
    -------
    list
        CLAs to be passed-thru to ASV.

    """
    flag_branch = package["flag"]["branch"]
    flag_commit = package["flag"]["commit"]

    dargs = {key: arg for key, arg in enumerate(args)}
    match = lambda arg: arg.startswith(flag_branch) or arg.startswith(flag_commit)
    keys = [key for key, arg in dargs.items() if match(arg)]

    if keys:
        for key in keys:
            arg = dargs.pop(key)
            lhs, rhs = arg.split("=")
            package["arg"][lhs] = rhs

        args = dargs.values()

    return args


def load_cache():
    """
    Read the nox/ASV cache json file.

    Returns
    -------
    dict
        The key/value cache.

    """
    fname = (ASV_BASE_DIR / CACHE_FILE).resolve(strict=True)
    with open(fname, "rb") as fi:
        cache = json.load(codecs.getreader("utf-8")(fi))
    return cache


def dump_cache(metric):
    """
    Write the relevant look-up table entries into the
    nox/ASV cache json file.

    Parameters
    ----------
    metric : str
        The name of the metric.

    """
    # Configure colorama sequences.
    cset = cr.Back.CYAN + cr.Fore.WHITE + cr.Style.BRIGHT
    creset = cr.Back.RESET + cr.Fore.RESET + cr.Style.NORMAL

    def process_branch_commit(package):
        if package["arg"]["branch"] is None:
            package["arg"]["branch"] = package["default"]["branch"]

        if package["arg"]["commit"] is None:
            package["arg"]["commit"] = package["default"]["commit"]

        if package["arg"]["commit"] == LATEST_COMMIT:
            url = f"{package['endpoint']}/branches/{package['arg']['branch']}"
            response = requests.get(url)
            response.raise_for_status()
            package["arg"]["commit"] = response.json()["commit"]["sha"]
        else:
            package["arg"]["branch"] = None
            url = f"{package['endpoint']}/git/commits/{package['arg']['commit']}"
            response = requests.get(url)
            response.raise_for_status()

    # Ensure that the branch and commit is valid for each package
    # in the look-up table.
    for package in lookup.keys():
        process_branch_commit(lookup[package])

    # Prepare the contents from the look-up table that
    # will be written into the nox/ASV cache.
    fname = ASV_BASE_DIR / CACHE_FILE
    content = dict()
    for package in lookup.keys():
        for lhs, rhs in lookup[package]["flag"].items():
            content[rhs] = lookup[package]["arg"][lhs]

    # Determine whether the requested branch and commit for each
    # package is different from the nox/ASV cache.
    if fname.is_file():
        # Read the cache.
        cache = load_cache()

        # Determine whether the ASV commit differs from
        # look-up table commit for the associated package.
        actual = {cache[flag] for flag in content.keys()}
        expected = set(content.values())
        install = actual != expected

        # Update the contents from the cache, but not
        # overwritting the requested branch and commits.
        [cache.pop(flag) for flag in content.keys()]
        content.update(cache)

        # Request ASV to re-install dependencies due to
        # a mismatch in commits.
        if install:
            print(
                f"{cset}{metric} > Request ASV re-installation of package dependencies.{creset}"
            )
            asv_env_dir = content.get(metric)
            if asv_env_dir is not None:
                asv_install_status = Path(asv_env_dir) / "asv-install-status.json"
                if asv_install_status.exists():
                    asv_install_status.unlink()
    else:
        print(
            f"{cset}{metric} > Request ASV new installation of package dependencies.{creset}"
        )

    # Reresh the nox/ASV cache.
    content["active"] = metric
    with open(fname, "wb") as fo:
        json.dump(content, codecs.getwriter("utf-8")(fo), indent=4, sort_keys=True)


def make_div(title=None):
    """
    Generate a terminal wide divider with optional title.

    Parameters
    ----------
    title : str or None
        An optional title to be positioned centrally within
        the divider.

    Returns
    -------
    str
        Formatted string divider.

    """
    if title is None:
        title = ""

    # Determine the terminal width.
    term = os.get_terminal_size()
    columns = term.columns

    # Configure colorama sequences.
    csb, csn = cr.Style.BRIGHT, cr.Style.NORMAL
    cfg, cfx = cr.Fore.GREEN, cr.Fore.RESET

    # Create the title, with collapsed spacing.
    if title:
        title = f"  session: {' '.join(title.strip().split())}  "

    # Embed the title within the terminal wide divider.
    n = len(title)
    hn = int((columns - n) / 2)
    ldiv = "=" * hn
    rdiv = ldiv if (2 * len(ldiv) + n) == columns else f"{ldiv}="
    div = f"{csb}{ldiv}{cfg}{title}{cfx}{rdiv}{csn}"

    return div


def report(session, commit_flag):
    """
    Generate, format and display a summary of the benchmark
    results for the nox session environment (metric).

    Parameters
    ----------
    session : object
        A `nox.sessions.Session` object.
    commit_flag : str
        Key to identify the package commit sha
        reference from the cache.

    """

    def todate(d):
        return datetime.fromtimestamp(d / 1000)

    def delta(start, end):
        return todate(end) - todate(start)

    metric_full = Path(session.virtualenv.location).name
    metric = metric_full.split("-")[0]
    hostname = os.environ["HOSTNAME"]
    machine_dir = (ASV_BASE_DIR / ".asv" / "results" / hostname).resolve(strict=True)

    # Determine terminal information.
    term = os.get_terminal_size()
    columns = term.columns

    # Configure colorama terminal sequences.
    csb, csn = cr.Style.BRIGHT, cr.Style.NORMAL
    cfg, cfr, cfx = cr.Fore.GREEN, cr.Fore.RED, cr.Fore.RESET

    # Re-read the cache, to include and ASV updates.
    cache = load_cache()

    # Load the ASV host machine details.
    commit = cache.get(ASV_COMMIT_FLAG, cache[commit_flag])
    machine_fname = (machine_dir / "machine.json").resolve(strict=True)
    with open(machine_fname, "rb") as fi:
        machine = json.load(codecs.getreader("utf-8")(fi))

    print(make_div(metric_full))

    # Compress any extraneous spacing within strings.
    for key in sorted(machine.keys()):
        value = " ".join(str(machine[key]).strip().split())
        print(f"{csb}{key}:{csn} {value}")

    # Load the ASV metric results for the commit.
    report_fname = (
        machine_dir / f"{commit[:8]}-conda-py{session.python}.json"
    ).resolve(strict=True)
    with open(report_fname, "rb") as fi:
        report = json.load(codecs.getreader("utf-8")(fi))

    date = datetime.fromtimestamp(report["date"] / 1000)
    print(f"{csb}date:{csn} {date}")
    print(f"{csb}python:{csn} {session.python}\n")

    # Filter out other metic test results.
    tests = [test for test in report["results"].keys() if test.startswith(metric)]

    # Create our beautiful table.
    precision = 6
    table = BeautifulTable(maxwidth=columns, detect_numerics=False)
    table.set_style(BeautifulTable.STYLE_BOX)
    header = [
        "Metric (time in seconds)",
        "Min",
        "Max",
        "Mean",
        "StdDev",
        "Median",
        "IQR†",
        "CI‡",
        "Number",
        "Repeat",
        "Duration",
    ]
    table.columns.header = [f"{cfr}{csb}{col}" for col in header]
    col_metric = table.columns.header[0]
    table.columns.alignment[col_metric] = BeautifulTable.ALIGN_LEFT
    nfailed = 0

    # Populate the table with each row per test (and parameterization).
    for test in tests:
        if report["results"][test] is None:
            name = ".".join(test.split(".")[1:])
            fail = f"{cfr}Failed"
            row = [f"{cfr}{name}"] + ([fail] * 10)
            table.rows.append(row)
            nfailed += 1
            continue
        params = report["results"][test].get("params")
        has_params = params is not None
        if not has_params:
            # Create a dummy single-valued parameter, as a convenience
            # for code simplicity.
            params = range(1)
        else:
            if len(params) > 1:
                # TBD: support metrics with multi-parameterization.
                raise ValueError(
                    "Report doesn't yet support multiple benchmark parameters."
                )
            params = params[0]
        result = report["results"][test]["result"]
        stats = report["results"][test]["stats"]

        for i, param in enumerate(params):
            name = f"{test}[{param}]" if has_params else test
            # Remove leading metric name from test.
            name = ".".join(name.split(".")[1:])
            duration = delta(report["started_at"][test], report["ended_at"][test])
            row = [
                f"{cfg}{name}",
                f"{csb}{stats[i]['min']:.{precision}f}{csn}",
                f"{csb}{stats[i]['max']:.{precision}f}{csn}",
                f"{csb}{stats[i]['mean']:.{precision}f}{csn}",
                f"{csb}{stats[i]['std']:.{precision}f}{csn}",
                f"{csb}{result[i]:.{precision}f}{csn}",
                f"{csb}{stats[i]['q_75'] - stats[i]['q_25']:.{precision}f}{csn}",
                f"{csb}{stats[i]['ci_99'][0]:.{precision}f};{stats[i]['ci_99'][1]:.{precision}f}{csn}",
                f"{csb}{stats[i]['number']}{csn}",
                f"{csb}{stats[i]['repeat']}{csn}",
                f"{csb}{duration.total_seconds():.3f}{csn}",
            ]
            table.rows.append(row)

    print(table)

    # Post-amble.
    ntests = len(table.rows)
    failures = f" ({nfailed} failure{'s' if nfailed>1 else ''})" if nfailed else ""
    print(f"\n{csb}benchmark:{csn} {ntests} test{'s' if ntests>1 else ''}{failures}\n")
    print(
        f"{cfr}{csb}† - IQR (InterQuartile Range) from 1st quartile and 3rd quartile."
    )
    print(f"‡ - 99% CI (Confidence Interval).{cfx}{csn}")
    print(make_div())


def benchmark(session, metric, packages=None):
    """
    Perform a suite of benchmarks for a specific metric.

    Parameters
    ----------
    session : object
        A `nox.sessions.Session` object.
    metric : string
        The name of the metric.
    packages : None or str or list of str
        pip or conda packages to be installed into
        the associated nox session environment.

    """
    # Initialise colorama.
    cr.init()

    # Default ASV run command for the benchmark.
    asv_command = ["asv", "run", "--bench", f"{metric}"]

    # Initialise the lookup table.
    for package in lookup.keys():
        lookup[package]["arg"]["branch"] = None
        lookup[package]["arg"]["commit"] = None

    # Install the requested packages into the nox environment.
    install(session, packages=packages)

    # Collect any CLAs.
    args = session.posargs if session.posargs else None

    # Parse the CLAs for branch/commit args and ASV args.
    if args is not None:
        asv_args = parse_cla(lookup["iris"], args)
        asv_command.extend(asv_args)

    # Write the nox/ASV cache.
    dump_cache(metric)

    # Run the ASV benchmark.
    session.cd(ASV_BASE_DIR)
    session.run(*asv_command)

    # Generate the metrics report.
    report(session, lookup["iris-ugrid"]["flag"]["commit"])

    # Uninitialise colorama.
    cr.deinit()


def setup_env(session):
    """
    Configure the session environment for metric testing.

    Parameters
    ----------
    session : object
        A `nox.sessions.Session` object.

    """
    if not DATA_CACHE_DIR.is_dir():
        DATA_CACHE_DIR.mkdir(parents=True)
    session.env.update(dict(NGVAT_SYNTHETIC_DATA_CACHEDIR=DATA_CACHE_DIR))


@nox.session(python=PY_VER, venv_backend="conda")
def sperf(session):
    """
    Perform SPerf metrics using ASV.

    Parameters
    ----------
    session : object
        A `nox.sessions.Session` object.

    """
    setup_env(session)
    benchmark(session, "sperf")


@nox.session(python=PY_VER, venv_backend="conda")
def cperf(session):
    """
    Perform CPerf metrics using ASV.

    Parameters
    ----------
    session : object
        A `nox.sessions.Session` object.

    """
    setup_env(session)
    benchmark(session, "cperf")
