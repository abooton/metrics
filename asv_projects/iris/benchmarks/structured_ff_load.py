"""
Test structured loading of a large-ish fieldsfile..

"""

from benchmarks import testdata_path
import iris
from iris.fileformats.um import structured_um_loading


class StructuredFFLoadBenchmark:
    params = ["structured", "unstructured"]

    def setup(self, structure):
        # Define the load path.
        self.file_path = testdata_path("umglaa_pb003-theta")

        # content one cube : <air_potential_temperature / (K)     (time: 3; model_level_number: 71; latitude: 1920; longitude: 2560)>
        # Structured load of this should show benefit over standard load, avoiding the cost of merging

        if structure == "structured":
            def load(file_path):
                with structured_um_loading():
                    iris.load(file_path)
        elif structure == "unstructured":
            def load(file_path):
                iris.load(file_path)
        else:
            raise NotImplementedError
        self.load = load

    def time_structured_load(self, structure):
        # Load the whole file (in fact there is only 1 cube).
        self.load(self.file_path)
