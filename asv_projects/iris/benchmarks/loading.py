"""
Loading benchmark tests.

"""

import tempfile
from benchmarks import testdata_path
import iris
import numpy as np


DATA_REALISATION_FNAMES = {
    'ff_uncompressed': 'umglaa_pb000-theta.uncompressed',
    'ff_compressed': 'umglaa_pb000-theta',
    'pp_uncompressed': 'umglaa_pb000-theta.pp',
    'pp_compressed': 'umglaa_pb000-theta.compressed.pp',
    'netCDF_uncompressed': 'umglaa_pb000-theta.nc',
    'netCDF_compressed': 'umglaa_pb000-theta.compressed.nc'
    }

STASH_CONSTRAINT_FNAMES = {
    'ff': 'umglaa_pb000-theta',
    'pp': 'umglaa_pb000-theta.pp',
    }

TIME_CONSTRAINT_FNAMES = {
    'ff': 'umglaa_pb003-theta',
    'pp': 'umglaa_pb003-theta.pp',
    'netCDF': 'umglaa_pb003-theta.nc',
    }


class DataRealisation:
    # Tests the performance of loading and realising (un)compressed data.
    params = DATA_REALISATION_FNAMES.keys()

    def setup(self, test_param):
        file_path = testdata_path(DATA_REALISATION_FNAMES[test_param])
        # Load the cube prior to the benchmark
        self.cube = iris.load_cube(file_path)

    def time_realise_data(self, fname):
        # Realise the data from a single 2D slice.
        self.cube[0].data


class STASHConstraint:
    # Tests the performance of loading a cube using a STASH Constraint.
    params = STASH_CONSTRAINT_FNAMES.keys()

    def setup(self, test_param):
        self.file_path = testdata_path(STASH_CONSTRAINT_FNAMES[test_param])

    def time_stash_constraint(self, test_param):
        cube = iris.load_cube(
                self.file_path, iris.AttributeConstraint(STASH='m01s00i004'))


class TimeConstraint:
    # Tests the performance of loading a cube using a constraint on the time
    # coordinate.
    params = TIME_CONSTRAINT_FNAMES.keys()

    def setup(self, test_param):
        self.file_path = testdata_path(TIME_CONSTRAINT_FNAMES[test_param])

    def time_time_constraint(self, test_param):
        # The files contain data for 1am, 2am and 3am.
        time_constr = iris.Constraint(time= lambda cell: cell.point.hour > 1)
        cube = iris.load_cube(self.file_path, time_constr)


class ManyVars:
    # Tests the loading of a file containing an unusually large number of
    # coordinates.
    params = ["nc"]

    def setup(self, test_param):
        self.temp_dir = tempfile.TemporaryDirectory()
        self.fname = self.temp_dir.name + "/many_var_file." + test_param

        data_len = 8
        data = np.arange(data_len)
        cube = iris.cube.Cube(data, units="unknown")
        extra_vars = 80
        names = ["coord_" + str(i) for i in range(extra_vars)]
        for name in names:
            coord = iris.coords.AuxCoord(data, long_name=name, units="unknown")
            cube.add_aux_coord(coord, 0)
        iris.save(cube, self.fname)

    def teardown(self, test_param):
        self.temp_dir.cleanup()

    def time_many_var_load(self, test_param):
        cubes = iris.load(self.fname)
