"""Common code for benchmarks."""
import os
from pathlib import Path

from iris_ugrid.tests.synthetic_data_generator import (
    create_file__xios_half_levels_faces,
)
from iris_ugrid.ugrid_cf_reader import load_cubes as load_ugrid_cubes


# Get the common directory used for synthetic data testfiles.
_SYNTHETIC_DATA_PATH = Path(
    os.environ.get("NGVAT_SYNTHETIC_DATA_CACHEDIR", "invalid-path")
).resolve()
if not _SYNTHETIC_DATA_PATH.exists() or not _SYNTHETIC_DATA_PATH.is_dir():
    raise ValueError(
        "Please set $NGVAT_SYNTHETIC_DATA_CACHEDIR to a valid path :\n"
        "   'export NGVAT_SYNTHETIC_DATA_CACHEDIR=<synthetic-data directory>'."
    )


def make_cubesphere_testfile(cube_size):
    """
    Build a C<cube_size> cubesphere testfile in a given directory, with a standard naming.
    Skip the actual build, if a file of the appropriate name already exists.
    Return the file path.

    """
    global _SYNTHETIC_DATA_PATH
    n_faces = 6 * cube_size * cube_size
    stem_name = f"mesh_cubesphere_C{cube_size}"
    file_path = (_SYNTHETIC_DATA_PATH / stem_name).with_suffix(".nc")
    if not file_path.exists():
        print(f"Creating new synthetic cubesphere file : {file_path}")
        new_path = create_file__xios_half_levels_faces(
            temp_file_dir=str(file_path.parent),  # N.B. function is not Path-aware.
            dataset_name=stem_name,  # N.B. function adds the ".nc" extension
            n_times=1,
            n_faces=n_faces,
        )
        if new_path != str(file_path) or not file_path.exists():
            raise ValueError("Failed to create file {file_path}")
    return file_path


def ugrid_load_cube(file_path, *load_args, **load_kwargs):
    """ A ugrid equivalent of 'iris.load_cube'."""
    # Cannot yet use "iris.load_xxx" functions.
    # Here's a thing that at least works, expecting a single cube result.
    loaded_cubes = list(load_ugrid_cubes(str(file_path), *load_args, **load_kwargs))
    (cube,) = loaded_cubes
    return cube
