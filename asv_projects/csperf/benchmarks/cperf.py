from iris import load_cube as iris_load_cube

from benchmarks import make_cubesphere_testfile, ugrid_load_cube


def loadargs_single_2d_diagnostic_UM():
    file_path = "/project/avd/ng-vat/data/sprint_05_20201112/umglaa_pa000-surfacetemp"
    args = [file_path]
    kwargs = {}
    return args, kwargs


def create_umsized_synthetic_datafile():
    # The data of the UM file noted above has dtype=np.float32 shape=(1920, 2560)
    # The closest cubesphere size in terms of datapoints is sqrt(1920*2560 / 6) ~= 905
    # I.E. "C905"
    cells_per_panel_edge = 905
    file_path = make_cubesphere_testfile(cells_per_panel_edge)
    return str(file_path)


def loadargs_single_2d_diagnostic_LFRic():
    file_path = create_umsized_synthetic_datafile()
    args = [file_path]
    kwargs = {}
    return args, kwargs


# Actual benchmarks


class Single2d_LoadCubes_UM:
    def setup(self):
        self.loadargs = loadargs_single_2d_diagnostic_UM()

    def time_loadcubes_UM(self):
        args, kwargs = self.loadargs
        cube = iris_load_cube(*args, **kwargs)
        assert cube.has_lazy_data()


class Single2d_RealiseCubedata_UM:
    # Prevent multiple benchmark calls within a timing iteration (aka a "repeat").
    # Because the benchmark realizes the cube, and needs it to be lazy to start with (which it checks).
    # To ensure this, we must turn off the warmup phase, and set number=1
    number = 1
    warmup_time = 0.0

    def setup(self):
        args, kwargs = loadargs_single_2d_diagnostic_UM()
        self.cube = iris_load_cube(*args, **kwargs)

    def time_realisedata_UM(self):
        assert self.cube.has_lazy_data()
        _ = self.cube.data
        assert not self.cube.has_lazy_data()


class Single2d_LoadCubes_LFRic:
    def setup(self):
        self.loadargs = loadargs_single_2d_diagnostic_LFRic()

    def time_loadcubes_LFRic(self):
        args, kwargs = self.loadargs
        cube = ugrid_load_cube(*args, **kwargs)
        assert cube.has_lazy_data()


class Single2d_RealiseCubedata_LFRic:
    # Prevent multiple benchmark calls within a timing iteration (aka a "repeat").
    # Because the benchmark realizes the cube, and needs it to be lazy to start with (which it checks).
    # To ensure this, we must turn off the warmup phase, and set number=1
    number = 1
    warmup_time = 0.0

    def setup(self):
        args, kwargs = loadargs_single_2d_diagnostic_LFRic()
        self.cube = ugrid_load_cube(*args, **kwargs)
        assert self.cube.has_lazy_data()

    def time_realisedata_LFRic(self):
        assert self.cube.has_lazy_data()
        _ = self.cube.data
        assert not self.cube.has_lazy_data()
