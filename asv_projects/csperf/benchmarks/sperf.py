from benchmarks import make_cubesphere_testfile, ugrid_load_cube


class Sperf_2d_Mixin:
    # Parameterise by cube sizes.
    # Numbers are in "C" notation, (e.g. C1 is 6 faces, 8 nodes)
    # TESTING NUMBERS ..
    # params = [4, 12, 1200 ]
    # OR .. as defined in AVD-1529
    params = [12, 384, 1280, 1668]

    # Make readable param names.
    param_names = ["cubesphere_C<N>"]


class Sperf_2d_Cubesphere_Load(Sperf_2d_Mixin):
    def setup(self, cube_size):
        self.file_path = make_cubesphere_testfile(cube_size)

    def time_load_2d_cube(self, cube_size):
        cube = ugrid_load_cube(self.file_path)
        assert cube.has_lazy_data()


class Sperf_2d_Cubesphere_Realise(Sperf_2d_Mixin):
    # Prevent multiple benchmark calls within a timing iteration (aka a "repeat").
    # Because the benchmark realizes the cube, and needs it to be lazy to start with (which it checks).
    # To ensure this, we must turn off the warmup phase, and set number=1
    number = 1
    warmup_time = 0.0

    def setup(self, cube_size):
        # As above, but this time also load the file as a cube, ready to time data-loading.
        file_path = make_cubesphere_testfile(cube_size)
        self.cube = ugrid_load_cube(file_path)

    def time_realise_2d_cube(self, cube_size):
        assert self.cube.has_lazy_data()
        self.cube.data
        assert not self.cube.has_lazy_data()
