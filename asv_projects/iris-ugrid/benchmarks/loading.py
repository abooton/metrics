"""
Loading benchmark tests.

Where possible benchmarks should be parameterised for two sizes of input data:
  * minimal: enables detection of regressions in parts of the run-time that do
             NOT scale with data size.
  * large: large enough to exclusively detect regressions in parts of the
           run-time that scale with data size. Aim for benchmark time ~20x
           that of the minimal benchmark.

"""

from pathlib import Path
from shutil import rmtree

from iris.cube import CubeList
from iris_ugrid.tests.synthetic_data_generator import create_file__xios_half_levels_faces
from iris_ugrid.ugrid_cf_reader import load_cubes

SYNTH_DATA_DIR = Path().cwd() / "tmp_data"


def setup(*args):
    SYNTH_DATA_DIR.mkdir(exist_ok=True)


def teardown(*args):
    rmtree(SYNTH_DATA_DIR)


def synthetic_data(**kwargs):
    # Ensure all uses of the synthetic data function use the common directory.
    return create_file__xios_half_levels_faces(temp_file_dir=SYNTH_DATA_DIR, **kwargs)


def load_cube(*args, **kwargs):
    # Note: cannot use iris.load, as merge can not yet handle UCubes.
    return CubeList(load_cubes(*args, **kwargs))[0]


class BasicLoading:
    params = [1, int(4.1e6)]
    param_names = ["number of faces"]

    def setup(self, *args):
        self.data_path = synthetic_data(dataset_name="Loading", n_faces=args[0])

    def time_load_file(self, *args):
        _ = load_cube(self.data_path)


class DataRealisation:
    # Prevent repeat runs between setup() runs - data won't be lazy after 1st.
    number = 1
    # Compensate for reduced certainty by increasing number of repeats.
    repeat = (10, 10, 10.0)
    # Prevent ASV running its warmup, which ignores `number` and would
    # therefore get a false idea of typical run time since the data would stop
    # being lazy.
    warmup_time = 0.0

    params = [1, int(4e6)]
    param_names = ["number of faces"]

    def setup(self, *args):
        data_path = synthetic_data(dataset_name="Realisation", n_faces=args[0])
        self.cube = load_cube(data_path)

    def time_realise_data(self, *args):
        assert self.cube.has_lazy_data()
        _ = self.cube.data[0]


class Callback:
    params = [1, int(4.5e6)]
    param_names = ["number of faces"]

    def setup(self, *args):
        def callback(cube, field, filename):
            return cube[::2]

        self.data_path = synthetic_data(dataset_name="Loading", n_faces=args[0])
        self.callback = callback

    def time_load_file_callback(self, *args):
        _ = load_cube(self.data_path, callback=self.callback)
