# Metrics

This repository contains the following types of benchmark tests for Iris:
* tests using [tehuti](https://github.com/SciTools/tehuti)
* tests using [Air Speed Velocity](https://asv.readthedocs.io/en/stable/) (ASV)

## Quickstart: Running the ASV tests locally
Running the tests requires a copy of
[iris-test-data](https://github.com/SciTools/iris-test-data), as well as a set
of extra data files that are specifically for running ASV tests.

#### Set up Iris test data
Check out a local copy of https://github.com/SciTools/iris-test-data and point
to it using
```
$ export OVERRIDE_TEST_DATA_REPOSITORY=/path/to/dir/containing/test_data
```
Note the above should point to a directory containg the `test_data` directory. 

#### Set up the extra data files
A copy of the extra data files are located in
`eldavdbuild03:/data/local/avd/asv/data`
as well as being archived to MASS:
`moose:/adhoc/projects/avd/asv/data_for_nightly_tests`.

Take a copy of these files and then point to the directory containing the 
files with the following
```
$ export BENCHMARK_DATA=/path/to/dir/containing/extra/data/files
```

#### Running the tests
The simplest way to run the tests is the following:
```
$ cd asv_projects/iris
$ asv run
```
This will run the tests in the `benchmarks` directory using the master branch
of Iris. For more information, see the
[ASV documentation](https://asv.readthedocs.io/en/stable/).
